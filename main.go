package main

import (
	"compress/gzip"
	"database/sql"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	fmt.Println("Hello World")
	db, err := sql.Open("sqlite3", "./NoteStore.sqlite")
	if err != nil {
		panic("Error on connection")
	}

	rows, err := db.Query("SELECT ZDATA FROM ZICNOTEDATA")
	if err != nil {
		panic("Error querying data")
	}
	run := strconv.FormatInt(time.Now().Unix(), 10)
	err = os.MkdirAll("Notes-"+run, os.ModePerm)
	if err != nil {
		panic("error making directory")
	}
	var data []byte
	i := 1
	var d []byte
	dir := "Notes-" + run + "/"
	final := dir + "final.txt"
	for rows.Next() {
		err := rows.Scan(&data)
		if err != nil {
			panic("error scanning data")
		}
		file := dir + "note" + strconv.Itoa(i) + ".txt"
		filename := file + ".gz"
		err = ioutil.WriteFile(filename, data, 0777)
		if err != nil {
			panic("error writing file")
		}
		gzipfile, err := os.Open(filename)
		if err != nil {
			panic("error opening file")
		}
		reader, err := gzip.NewReader(gzipfile)
		if err != nil {
			panic("error opening file")
		}
		defer reader.Close()
		newfilename := strings.TrimSuffix(filename, ".gz")
		emptyFile, err := os.Create(final)
		if err != nil {
			panic("cant create final file")
		}
		emptyFile.Close()
		writer, err := os.Create(newfilename)

		if err != nil {
			panic("error creating file")
		}
		defer writer.Close()
		if _, err = io.Copy(writer, reader); err != nil {
			panic("error copying file")
		}
		f, err := os.Open(file)
		if err != nil {
			panic("error opening real file")
		}
		data, err := ioutil.ReadAll(f)
		if err != nil {
			panic("error reading all from file")
		}
		for _, v := range data {
			if v == 10 || v == 11 || (v >= 32 && v <= 127) {
				d = append(d, v)
			}
		}
		err = os.Remove(filename)
		if err != nil {
			log.Fatal(err)
		}
		err = os.Remove(newfilename)
		if err != nil {
			log.Fatal(err)
		}
		i = i + 1
	}
	file, err := os.OpenFile(final, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0777)

	if err != nil {
		fmt.Println("Could not open example.txt")
		return
	}

	defer file.Close()

	_, err2 := file.WriteString(string(d))

	if err2 != nil {
		fmt.Println("Could not write text to example.txt")

	}
}
